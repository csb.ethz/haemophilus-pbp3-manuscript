# Information
### This folder contains the _Electronic Supplementary Material_ of the folowing manuscript:
Josiane Reist, Janina Linnik, Urs Schibli, Adrian Egli & Vladimira Hinić*, "Impact of different amino acid substitutions in penicillin-binding protein 3 on beta-lactam susceptibility in Haemophilus influenzae".

*Corresponding author.
Division of Clinical Microbiology, University Hospital Basel, Switzerland.
E-mail: <vladimira.hinic@usb.ch>

# Content
* `data_disc_diameter.csv`: Benzylpenicillin screen data for all isolated strains.
* `data_etest_mic.csv`: Minimal inhibitory concentrations (MICs) of all isolated strains for tested beta-lactam antibiotics.
* `data_etest_mic_processed.csv`: Tidy data format of `data_etest_mic.csv`.
* `1_process_data.R`: Script to produce `data_etest_mic_processed.csv`.
* `2_plot_mic_data.R`: Script to reproduce __Fig. S1__.
* `2_wilcox_test.R`: Statistical analysis of MIC values and script to reproduce __Fig. 1__.
* `plot_diameter_data.R`: Script to reproduce __Fig. S2__.
* `theme_plot.R`: Ggplot theme that specifies figure style.
* `ESM3_annotated_sequence_alignment.txt`: Annotated sequence alignment between _H. influenzae_ RD KW20 (UniProt entry P45059) and _P. aeruginosa_ (UniProt entry Q51504)

# Requirements
* _R_ version >= 3.5
* _R_ packages: ggsignif (0.4),  ggpubr (0.2),  magrittr (1.5), cowplot (0.9), ggplot2 (3.1), here (0.1), readr (1.3), tidyr (0.8), dplyr (0.8)

# Authors
* Josiane Reist <josiane.reist@usb.ch> (Data)
* Janina Linnik <janina.linnik@bsse.ethz.ch> (Scripts)

# Licence
Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php

# References:
* Preprint available: https://www.biorxiv.org/content/10.1101/846428v1
